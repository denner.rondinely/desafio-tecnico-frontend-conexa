import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import TextField, { TextFieldProps } from '.';

export default {
  title: 'TextField',
  component: TextField,
  args: {
    label: 'Nome',
    name: 'nome',
    placeholder: 'Digite seu nome!',
    disabled: false
  },
  argTypes: {
    onInput: { action: 'onInput' }
  }
} as Meta;

export const Default: Story<TextFieldProps> = (args) => (
  <div style={{ maxWidth: 500, padding: 15 }}>
    <TextField {...args} />
  </div>
);

export const withError: Story<TextFieldProps> = (args) => (
  <div style={{ maxWidth: 300, padding: 15 }}>
    <TextField {...args} />
  </div>
);

withError.args = {
  error: 'Ocorreu um erro'
};

export const withDisabled: Story<TextFieldProps> = (args) => (
  <div style={{ maxWidth: 300, padding: 15 }}>
    <TextField {...args} />
  </div>
);

withDisabled.args = {
  disabled: true
};

export const typeNumber: Story<TextFieldProps> = (args) => (
  <div style={{ maxWidth: 300, padding: 15 }}>
    <TextField {...args} />
  </div>
);

typeNumber.args = {
  type: 'number',
  id: 'teste'
};
