import React from 'react';

import Button from 'components/Button';
import Icon from 'components/Icon';
import Typography from 'components/Typography';

import * as S from './styles';

export type HeaderProps = {
  name?: string;
  hasMenu?: boolean;
  logout?: () => void;
};

const Header = ({ name, hasMenu, logout }: HeaderProps) => {
  const handleLogout = () => logout && logout();
  return (
    <S.Wrapper>
      <S.Container hasMenu={hasMenu}>
        <S.LogoWrapper>
          <Icon icon="logo" />
        </S.LogoWrapper>
        {hasMenu && (
          <S.MenuWrapper>
            <Typography>Olá, {name}</Typography>

            <Button size="small" border onClick={handleLogout}>
              Sair
            </Button>
          </S.MenuWrapper>
        )}
      </S.Container>
    </S.Wrapper>
  );
};

export default Header;
