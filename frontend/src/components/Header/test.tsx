import React from 'react';
import { fireEvent, render, screen } from 'utils/tests/helpers';

import Header from '.';

describe('<Header />', () => {
  it('should render header', () => {
    const { container } = render(<Header hasMenu />);

    expect(screen.getByRole('button', { name: /sair/i })).toBeInTheDocument();

    expect(container.firstChild).toMatchSnapshot();
  });

  it('should to call onInputUser with typed text', async () => {
    const logout = jest.fn();
    const { container } = render(<Header logout={logout} hasMenu />);

    fireEvent.click(screen.getByRole('button', { name: /sair/i }));

    expect(logout).toHaveBeenCalled();

    expect(container.firstChild).toMatchSnapshot();
  });
});
