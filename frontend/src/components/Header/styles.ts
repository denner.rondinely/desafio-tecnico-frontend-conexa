import styled, { css } from 'styled-components';
import media from 'styled-media-query';
import { HeaderProps } from '.';

type WrapperProps = Pick<HeaderProps, 'hasMenu'>;

export const Wrapper = styled.header`
  ${({ theme }) => css`
    width: 100%;
    padding: ${theme.spacings.xsmall};
    background-color: ${theme.colors.white};
    box-shadow: 4px 4px 12px rgba(0, 0, 0, 0.05);
  `}
`;

export const Container = styled.div<WrapperProps>`
  ${({ hasMenu }) => css`
    width: 100%;
    max-width: 136.6rem;
    display: flex;
    align-items: center;
    justify-content: ${hasMenu ? 'space-between' : 'center'};
    margin: 0 auto;

    ${media.greaterThan('medium')`
      justify-content: space-between;
    `}
  `}
`;

export const LogoWrapper = styled.div`
  ${({ theme }) => css`
    padding: ${theme.spacings.minimal} 0;
    display: flex;
    align-items: center;
    > svg {
      width: 14.1rem;
      height: 2.4rem;
    }
  `}
`;

export const MenuWrapper = styled.div`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    gap: ${theme.spacings.medium};

    > *:first-child {
      display: none;
    }

    ${media.greaterThan('medium')`
      > *:first-child {
        display: block;
      }
    `}
  `}
`;
