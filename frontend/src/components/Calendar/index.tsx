import React, { useEffect, useState } from 'react';
import {
  getDaysInMonth,
  startOfMonth,
  addMonths,
  subMonths,
  setDate
} from 'date-fns';

import Icon from 'components/Icon';
import Typography from 'components/Typography';
import Button from 'components/Button';

import { formatDate } from 'utils/format-date';

import * as S from './styles';

type CalendarYearMonth = {
  year: number;
  month: number;
};

export type CalendarProps = {
  initialDay?: Date;
  onMonth?(e: CalendarYearMonth): void;
  onDay?(e: Date): void;
};

const Calendar = ({ initialDay, onDay, onMonth }: CalendarProps) => {
  const today = new Date();
  const [date, setNewDate] = useState<Date>(initialDay ?? today);
  const [day, setDay] = useState<Date>(date);

  const [month, setMonth] = useState(date.getMonth());
  const [year, setYear] = useState(date.getFullYear());
  const [weekDay, setWeekDay] = useState({
    month,
    week: 0
  });
  const [monthDays, setMonthDays] = useState<Array<number | string>>([]);
  const DAYS_OF_THE_WEEK = ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'];

  const handleSetNewDate = (d: number | string, i: number) => {
    const newDate = setDate(date, Number(d));
    setNewDate(newDate);
    setWeekDay({
      month,
      week: i % 7
    });
    setDay(newDate);
    onDay && onDay(newDate);
  };

  const previous = () => {
    const newDate = subMonths(date, 1);
    setNewDate(newDate);
    onMonth &&
      onMonth({
        month: newDate.getMonth(),
        year: newDate.getFullYear()
      });
  };

  const next = () => {
    const newDate = addMonths(date, 1);
    setNewDate(newDate);
    onMonth &&
      onMonth({
        month: newDate.getMonth(),
        year: newDate.getFullYear()
      });
  };

  const handleWeekDay = (e: number): boolean =>
    weekDay.week === e && weekDay.month === month ? true : false;

  const validDay = (d: number) =>
    d === day?.getDate() && day?.getMonth() === month;

  useEffect(() => {
    setYear(date.getFullYear());
    setMonth(date.getMonth());
  }, [date, onMonth]);

  useEffect(() => {
    const getMonthDays = () => {
      const daysInMonth = getDaysInMonth(new Date(year, month, 1));
      const firstDayOfMonthIndex = startOfMonth(
        new Date(year, month, 1)
      ).getDay();

      let arr: Array<number | string> = [];
      const tempMonthDays: Array<number | string> = Array.from(
        Array(firstDayOfMonthIndex),
        () => ''
      );

      Array.from(Array(daysInMonth), (_, i) => i + 1).forEach((d, index) => {
        tempMonthDays.push(d);
        if (tempMonthDays.length % DAYS_OF_THE_WEEK.length === 0) {
          arr = tempMonthDays;
        } else if (index + 1 === daysInMonth) {
          arr = tempMonthDays;
        }
      });

      setMonthDays(arr);
    };
    getMonthDays();
  }, [DAYS_OF_THE_WEEK.length, month, year]);

  return (
    <S.Wrapper>
      <S.Header>
        <Button
          data-testid="previous"
          bg="transparent"
          onClick={previous}
          icon={<Icon icon="chevron-left-solid" color="blue-light" size={10} />}
        />
        <Typography variant="caption" component="span">
          {formatDate(date, 'MMM yyyy')}
        </Typography>
        <Button
          data-testid="next"
          onClick={next}
          bg="transparent"
          icon={
            <Icon icon="chevron-right-solid" color="blue-light" size={10} />
          }
        />
      </S.Header>
      <S.Content>
        <S.WeekWrapper>
          {DAYS_OF_THE_WEEK.map((d, i) => (
            <S.WeekDay key={i}>
              <Typography
                variant="body"
                color={handleWeekDay(i) ? 'gray-dark' : 'gray'}
              >
                {d}
              </Typography>
            </S.WeekDay>
          ))}
        </S.WeekWrapper>
        <S.MonthWrapper>
          {monthDays.map((d, index) => {
            return (
              <S.MonthDay
                key={index}
                select={validDay(d as number)}
                onClick={() => d && handleSetNewDate(d, index)}
              >
                <Typography
                  component="span"
                  variant="caption"
                  color={validDay(d as number) ? 'white' : 'gray'}
                >
                  {d > 0 ? d : ''}
                </Typography>
              </S.MonthDay>
            );
          })}
        </S.MonthWrapper>
      </S.Content>
    </S.Wrapper>
  );
};

export default Calendar;
