import styled, { css } from 'styled-components';

type MonthDayProps = {
  select?: boolean;
};

export const Wrapper = styled.div`
  ${({ theme }) =>
    css`
      display: inline-flex;
      flex-direction: column;
      background-color: ${theme.colors.white};
      padding: ${theme.spacings.small};
      border-radius: ${theme.border.radius.medium};
    `}
`;

export const Header = styled.div`
  ${({ theme }) => css`
    display: flex;
    justify-content: center;
    align-items: center;
    border-bottom: 0.1rem solid ${theme.colors['gray-light']};
    padding-bottom: ${theme.spacings.minimal};
    > * {
      &:not(:last-child) {
        margin-right: ${theme.spacings.medium};
      }
    }
  `}
`;

export const Content = styled.div`
  padding: ${({ theme }) => theme.spacings.xsmall} 0;
  display: flex;
  flex-direction: column;
  > * {
    &:not(:last-child) {
      margin-bottom: ${({ theme }) => theme.spacings.xxsmall};
    }
  }
`;

export const WeekWrapper = styled.div`
  ${({ theme }) => css`
    display: grid;
    grid-template-columns: repeat(7, 27px);
    grid-template-rows: 27px;
    justify-content: center;
    gap: ${theme.spacings.xsmall};
  `}
`;

export const WeekDay = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  background-color: ${({ theme }) => theme.colors.transparent};
  cursor: pointer;
  padding: 0;
  border: none;
`;

export const MonthWrapper = styled.div`
  ${({ theme }) => css`
    display: grid;
    grid-template-columns: repeat(7, 27px);
    grid-auto-rows: 27px;
    justify-content: center;
    gap: ${theme.spacings.xsmall};
  `}
`;

export const MonthDay = styled.button<MonthDayProps>`
  ${({ theme }) => css`
    position: relative;
    background-color: ${theme.colors.transparent};
    padding: 0;
    border: none;
    cursor: pointer;
  `}

  ${({ theme, select }) =>
    select &&
    css`
      background-color: ${theme.colors['blue-light']};
      border-radius: 50%;
    `}
`;

export const Footer = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
`;

export const SelectButton = styled.button`
  ${({ theme }) => css`
    background-color: ${theme.colors['blue-dark']};
    border: none;
    padding: ${theme.spacings.xxsmall} ${theme.spacings.large};
    border-radius: 4rem;
    cursor: pointer;
  `}
`;
