import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import Calendar, { CalendarProps } from './index';

export default {
  title: 'Calendar',
  component: Calendar
} as Meta;

export const Default: Story<CalendarProps> = (args) => <Calendar {...args} />;
Default.argTypes = {
  initialDay: {
    type: ''
  },
  onMonth: {
    action: 'onMonth'
  },
  onDay: {
    action: 'onDay'
  }
};
