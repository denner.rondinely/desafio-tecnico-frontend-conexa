import React from 'react';
import { fireEvent, screen } from '@testing-library/react';

import { render } from 'utils/tests/helpers';

import Calendar from './index';

describe('<Calendar />', () => {
  it('should render the Calendar', () => {
    render(<Calendar />);

    expect(screen.getByRole('button', { name: /28/i })).toBeInTheDocument();
  });

  it('should be called onDay', () => {
    const onDay = jest.fn();
    render(<Calendar onDay={onDay} />);

    fireEvent.click(screen.getByRole('button', { name: /28/i }));

    expect(onDay).toBeCalled();
  });

  it('should be called onMonth next click', () => {
    const onMonth = jest.fn();
    render(<Calendar onMonth={onMonth} />);

    fireEvent.click(screen.getByTestId('next'));

    expect(onMonth).toBeCalled();
  });

  it('should be called onmonth previous click', () => {
    const onMonth = jest.fn();
    render(<Calendar onMonth={onMonth} />);

    fireEvent.click(screen.getByTestId('previous'));

    expect(onMonth).toBeCalled();
  });
});
