import React from 'react';

import { fireEvent, screen } from '@testing-library/react';

import { render } from 'utils/tests/helpers';

import DropDown from './index';

const mock = [
  { name: 'Todos', value: 1 },
  { name: 'Documentação', value: 2 },
  { name: 'Validação', value: 3 }
];

describe('<DropDown />', () => {
  it('should render the heading', () => {
    const { container } = render(<DropDown options={mock} />);

    expect(screen.getByText('Documentação')).toBeInTheDocument();

    expect(container.firstChild).toMatchSnapshot();
  });

  it('should render with info', () => {
    const { container } = render(<DropDown options={mock} info="100" />);

    expect(container.firstChild).toContainHTML('span');

    expect(container.firstChild).toMatchSnapshot();
  });

  it('should render text before event', () => {
    const { container } = render(<DropDown options={mock} info="100" />);

    fireEvent.mouseMove(container);
    fireEvent.mouseOut(container);
    fireEvent.click(screen.getByText('Documentação'));
    expect(container.firstChild).toHaveTextContent('Documentação');
  });
});
