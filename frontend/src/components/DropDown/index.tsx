import React, { useState } from 'react';

import Icon from '../Icon';

import * as S from './styles';

type OptionType = {
  name: string;
  value: string | number;
};

export type DropDownProps = {
  options: OptionType[];
  info?: string;
  all?: string;
  label: string;
  fullWidth?: boolean;
  effect?: 'default' | 'effect';
  icon?: JSX.Element;
  onOption?(value: string | number): void;
};

const DropDown = ({
  options,
  info,
  all,
  label,
  fullWidth,
  icon,
  effect = 'default',
  onOption
}: DropDownProps) => {
  const [selected, setSelected] = useState<string>(label);
  const [active, setActive] = useState<boolean>(false);

  const handleOption = (option: OptionType) => {
    setSelected(all && label && option.name === all ? label : option.name);
    setActive(false);
    onOption && onOption(option.value);
  };

  return (
    <S.Wrapper
      effect={effect}
      active={active}
      fullWidth={fullWidth}
      onMouseOut={() => setActive(false)}
      onMouseMove={() => setActive(true)}
    >
      {!!icon && icon}
      <S.Selected>{selected}</S.Selected>
      <S.Info hasInfo={!!info}>{info}</S.Info>
      {options.length > 1 && (
        <Icon icon="chevron-down" size={9} color="gray-dark" />
      )}
      <S.Options active={active && options.length > 1}>
        {options.map((option: OptionType, index) => (
          <li key={index} onClick={() => handleOption(option)}>
            {option.name}
          </li>
        ))}
      </S.Options>
    </S.Wrapper>
  );
};

export default DropDown;
