import styled, { css, DefaultTheme } from 'styled-components';
import { rgba } from 'polished';

import { DropDownProps } from '.';

type OptionsProps = {
  active: boolean;
};

type WrapperProps = Pick<DropDownProps, 'effect' | 'fullWidth'> &
  Pick<OptionsProps, 'active'>;

type InfoProps = {
  hasInfo: boolean;
};

const wrapperModifiers = {
  default: (theme: DefaultTheme, active: boolean) => css`
    background-color: ${theme.colors.transparent};
    color: ${active ? theme.colors['blue-light'] : theme.colors['gray']};
    border-radius: ${theme.border.radius.small};
    font-size: ${theme.font.sizes['body']};
    line-height: ${theme.font.sizes['body']};
    font-weight: ${theme.font.normal};
    border: 0.1rem solid ${theme.colors.transparent};
    padding: 0;
  `,
  effect: (theme: DefaultTheme, active: boolean) => css`
    height: ${theme.spacings.large};
    background-color: ${active ? theme.colors.white : theme.colors.transparent};
    padding: ${theme.spacings.xxsmall} ${theme.spacings.xsmall};
    color: ${active ? theme.colors['blue-light'] : theme.colors['gray']};
    border-radius: ${theme.border.radius.small};
    font-size: ${theme.font.sizes.xsmall};
    line-height: ${theme.font.sizes.xsmall};
    font-weight: ${theme.font.bold};
    box-shadow: 0px 1px 3px
      ${active
        ? `${rgba(theme.colors.blue, 0.25)}, 0px 4px 21px -9px ${rgba(
            theme.colors.blue,
            0.25
          )};`
        : theme.colors.transparent};
    border: 0.1rem solid
      ${active ? theme.colors.transparent : theme.colors['gray-light-3']};
  `
};

export const Wrapper = styled.div<WrapperProps>`
  display: inline-flex;
  align-items: center;
  position: relative;
  transition: all 0.3s;
  cursor: pointer;
  ${({ theme, active, fullWidth, effect = 'default' }) => css`
    ${wrapperModifiers[effect](theme, active)};
    justify-content: ${fullWidth ? 'space-between' : 'center'};
    > *:not(:last-child) {
      margin-left: ${theme.spacings.xxsmall};
    }
  `}
`;

export const Selected = styled.span``;

export const Info = styled.span<InfoProps>`
  ${({ hasInfo, theme }) => css`
    display: ${hasInfo ? 'block' : 'none'};
    margin-left: ${theme.spacings.xxsmall};
    font-size: ${theme.font.sizes.xxsmall};
    line-height: ${theme.font.sizes.xxsmall};
    font-weight: ${theme.font.normal};
    color: ${theme.colors['gray']};
  `};
`;

export const Options = styled.ul<OptionsProps>`
  list-style: none;
  position: absolute;
  border-radius: 0.8rem;
  left: 0;
  white-space: nowrap;
  box-shadow: 0px 6px 40px rgba(0, 0, 0, 0.12);
  width: 100%;
  min-width: fit-content;
  ${({ active, theme }) => css`
    padding: ${theme.spacings.small} ${theme.spacings.medium};
    top: calc(100% + ${theme.spacings.xxsmall});
    display: ${active ? 'block' : 'none'};
    background-color: ${theme.colors.white};
    color: ${theme.colors['gray']};
    z-index: ${theme.layers.overlay};
    &::before {
      content: '';
      position: absolute;
      left: 0;
      height: ${theme.spacings.xxsmall};
      width: 100%;
      background-color: ${theme.colors.transparent};
      top: -${theme.spacings.xxsmall};
    }
    > li {
      transition: color 0.3s;
      font-size: ${theme.font.sizes.xsmall};
      line-height: ${theme.font.sizes.xsmall};
      font-weight: ${theme.font.bold};
      white-space: nowrap;
      width: 100%;

      &:not(:last-child) {
        margin-bottom: ${theme.spacings.xsmall};
      }
      &:hover {
        color: ${theme.colors.blue};
      }
    }
  `}
`;
