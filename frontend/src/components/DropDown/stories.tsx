import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import DropDown from './index';
import { DropDownProps } from '.';
import Icon from 'components/Icon';

export default {
  title: 'DropDown',
  component: DropDown,
  args: {
    options: [
      { name: 'Todos', value: 1 },
      { name: 'Documentação', value: 2 },
      { name: 'Validação', value: 3 }
    ]
  },
  argTypes: {
    options: {
      type: ''
    }
  }
} as Meta;

export const Default: Story<DropDownProps> = (args) => <DropDown {...args} />;

export const withIcon: Story<DropDownProps> = (args) => <DropDown {...args} />;

withIcon.args = {
  icon: <Icon color="black" size={15} icon="help" />
};

export const Effect: Story<DropDownProps> = (args) => <DropDown {...args} />;

Effect.args = {
  effect: 'effect'
};

export const withInfo: Story<DropDownProps> = (args) => <DropDown {...args} />;

withInfo.args = {
  info: '100',
  effect: 'effect'
};
