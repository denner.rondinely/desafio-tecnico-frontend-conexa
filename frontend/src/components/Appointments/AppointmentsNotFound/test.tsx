import React from 'react';

import { render } from 'utils/tests/helpers';

import AppointmentsNotFound from '.';

describe('<AppointmentsNotFound />', () => {
  it('should render AppointmentsNotFound', () => {
    const { container } = render(<AppointmentsNotFound />);

    expect(container.firstChild).toMatchSnapshot();
  });
});
