import styled from 'styled-components';
import media from 'styled-media-query';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  max-width: 52rem;
  margin: 0 auto;
`;
export const CertificatesWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin: 0 0 ${({ theme }) => theme.spacings.large};

  > svg {
    width: 9rem;
    height: 6rem;
  }

  ${media.greaterThan('large')`
    margin: 0 0 ${({ theme }) => theme.spacings.xxlarge};
  `}
`;

export const TextWrapper = styled.div`
  ${media.greaterThan('large')`
    margin: 0 0 ${({ theme }) => theme.spacings.large};
  `}
`;

export const PlantWrapper = styled.div`
  > svg {
    width: 8.1rem;
    height: 8.9rem;
  }
`;
