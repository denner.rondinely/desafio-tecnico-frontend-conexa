import React from 'react';

import Icon from 'components/Icon';
import Typography from 'components/Typography';

import * as S from './styles';

const AppointmentsNotFound = () => {
  return (
    <S.Wrapper>
      <S.CertificatesWrapper>
        <Icon icon="certificates" />
      </S.CertificatesWrapper>
      <S.TextWrapper>
        <Typography variant="subtitle">
          Não há nenhuma <br />
          consulta agendada
        </Typography>
      </S.TextWrapper>
      <S.PlantWrapper>
        <Icon icon="plant" />
      </S.PlantWrapper>
    </S.Wrapper>
  );
};

export default AppointmentsNotFound;
