import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import AppointmentsnotFound from '.';

export default {
  title: 'AppointmentsnotFound',
  component: AppointmentsnotFound
} as Meta;

export const Default: Story = (args) => <AppointmentsnotFound {...args} />;
