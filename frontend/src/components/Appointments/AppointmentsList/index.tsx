import React from 'react';

import Button from 'components/Button';
import Typography from 'components/Typography';

import { findOrDefault } from 'utils/findOrDefault';
import { Appointment } from 'types/appointments';

import * as S from './styles';

export type AppointmentsListProps = {
  appointments: Appointment[];
  onAttend?: (consultationId: number) => void;
};

const AppointmentsList = ({
  appointments,
  onAttend
}: AppointmentsListProps) => {
  const title = findOrDefault(appointments.length, {
    1: `${appointments.length} consulta agendada`,
    _: `${appointments.length} consultas agendadas`
  });
  return (
    <S.Wrapper>
      <Typography role="paragraph">{title}</Typography>
      <S.List>
        {appointments.map(({ name, date, consultationId }) => (
          <S.ListItem key={consultationId}>
            <S.Info>
              <Typography variant="body" nowrap>
                {name}
              </Typography>
              <Typography variant="caption" component="span">
                {date}
              </Typography>
            </S.Info>
            <Button onClick={() => onAttend && onAttend(consultationId)}>
              Atender
            </Button>
          </S.ListItem>
        ))}
      </S.List>
    </S.Wrapper>
  );
};

export default AppointmentsList;
