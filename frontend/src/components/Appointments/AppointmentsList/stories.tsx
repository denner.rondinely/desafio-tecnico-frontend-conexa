import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import AppointmentsList, { AppointmentsListProps } from '.';
import { appointments } from 'mock/appointments';

export default {
  title: 'AppointmentsList',
  component: AppointmentsList,
  argTypes: {
    appointments: {
      type: ''
    },
    onAttend: { action: 'onAttend' }
  }
} as Meta;

export const Default: Story<AppointmentsListProps> = (args) => (
  <AppointmentsList {...args} />
);

Default.args = {
  appointments
};
