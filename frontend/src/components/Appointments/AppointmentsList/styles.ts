import styled, { css } from 'styled-components';
import media from 'styled-media-query';

export const Wrapper = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;
    gap: ${theme.spacings.huge};
    width: 100%;
    max-width: 52rem;
    padding: ${theme.spacings.small} 0 0;

    > p {
      font-weight: ${theme.font.bold};
    }

    ${media.greaterThan('large')`
      gap: ${theme.spacings.large};
    `}
  `}
`;

export const List = styled.ul`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;
    gap: ${theme.spacings.xxlarge};
  `}
`;

export const ListItem = styled.li`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const Info = styled.div`
  display: flex;
  flex-direction: column;
`;
