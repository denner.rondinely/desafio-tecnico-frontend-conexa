import React from 'react';
import { fireEvent, render, screen } from 'utils/tests/helpers';

import { appointments } from 'mock/appointments';

import AppointmentsList from '.';

describe('<AppointmentsList />', () => {
  it('should render AppointmentsList', () => {
    const { container } = render(
      <AppointmentsList appointments={appointments} />
    );

    expect(screen.getByRole('list')).toBeInTheDocument();

    expect(container.firstChild).toMatchSnapshot();
  });

  it('should to call onAttend', async () => {
    const onAttend = jest.fn();
    const { container } = render(
      <AppointmentsList
        onAttend={onAttend}
        appointments={[
          { name: 'Teste', consultationId: 1, date: '10/12/2021 às 10:20' }
        ]}
      />
    );

    fireEvent.click(screen.getByRole('button', { name: /Atender/i }));

    expect(onAttend).toHaveBeenCalledWith(1);

    expect(container.firstChild).toMatchSnapshot();
  });

  it('should to render simple title', async () => {
    render(
      <AppointmentsList
        appointments={[
          { name: 'Teste', consultationId: 1, date: '10/12/2021 às 10:20' }
        ]}
      />
    );

    expect(screen.getByRole('paragraph')).toHaveTextContent(
      '1 consulta agendada'
    );
  });

  it('should to render title of multiple items', async () => {
    render(<AppointmentsList appointments={appointments} />);

    expect(screen.getByRole('paragraph')).toHaveTextContent(
      '3 consultas agendadas'
    );
  });
});
