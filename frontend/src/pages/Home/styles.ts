import styled, { css } from 'styled-components';
import media from 'styled-media-query';

export const Wrapper = styled.div``;

export const Header = styled.header`
  ${({ theme }) => css`
    padding: ${theme.spacings.small};
    ${media.greaterThan('large')`
      padding: ${theme.spacings.xxlarge} ${theme.spacings.xlarge};
    `}
  `}
`;

export const Main = styled.main`
  ${({ theme }) => css`
    padding: 0 ${theme.spacings.large};
  `}
`;

export const NotFoundWrapper = styled.div`
  ${media.greaterThan('large')`
    margin: 9.5rem 0 0;
  `}
`;

export const AppointmentsWrapper = styled.div`
  ${media.greaterThan('large')`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: ${({ theme }) => theme.spacings.xxhuge} 0 0;
  `}
`;

export const ScheduleWrapper = styled.div`
  display: flex;
  flex-direction: column;
  ${({ theme }) => css`
    gap: ${theme.spacings.xxlarge};
    margin: ${theme.spacings.xxsmall} 0 0;
  `}
`;

export const CalendarWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const SelectWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: ${({ theme }) => theme.spacings.medium};
`;

export const Footer = styled.footer`
  ${({ theme }) => css`
    display: flex;
    justify-content: space-between;
    padding: ${theme.spacings.small};
    border-top: 0.2rem solid ${theme.colors.gray};
    position: sticky;
    top: 100%;

    ${media.greaterThan('large')`
      border-top: none;
    `}
  `}
`;

export const HelpWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: ${({ theme }) => theme.spacings.xxlarge};
`;

export const ContactsWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: ${({ theme }) => theme.spacings.large};

  ${media.greaterThan('large')`
    justify-content: space-between;
  `}
`;

export const ContactsBox = styled.div`
  display: flex;
  flex-direction: column;
  gap: ${({ theme }) => theme.spacings.xxsmall};
`;

export const LoadingWrapper = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
`;
