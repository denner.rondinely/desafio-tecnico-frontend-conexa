import React, { useEffect, useState } from 'react';
import { toast } from 'react-toastify';

import Calendar from 'components/Calendar';
import AppointmentsNotFound from 'components/Appointments/AppointmentsNotFound';
import Button from 'components/Button';
import Typography from 'components/Typography';
import AppointmentsList from 'components/Appointments/AppointmentsList';
import Modal from 'components/Modal';
import DropDown from 'components/DropDown';

import api from 'services/api';
import { appointmentMapper } from 'utils/mappers/appointmentMapper';
import { patientMapper } from 'utils/mappers/patientMapper';
import { findOrDefault } from 'utils/findOrDefault';
import { errorMap } from 'utils/errorMap';

import { Appointment, Patient } from 'types/appointments';

import * as S from './styles';
import Loading from 'components/Loading';

const Home = () => {
  const [openSchedule, setOpenSchedule] = useState(false);
  const [loading, setLoading] = useState(false);
  const [scheduleDate, setScheduleDate] = useState<Date>(new Date());
  const [patientId, setPatientId] = useState<number>();
  const [openHelp, setOpenHelp] = useState(false);
  const [appointments, setAppointments] = useState<Appointment[] | null>(null);
  const [patients, setPatients] = useState<Patient[]>([]);

  const getAppointments = async () => {
    setLoading(true);
    try {
      const resp = await api.getAppointments();
      if (!resp.message && resp.length) {
        const rawAppointments = appointmentMapper(resp);
        const rawPatients = patientMapper(resp);
        setAppointments(rawAppointments);
        setPatients(rawPatients);
      } else {
        const message = findOrDefault(resp.message, {
          ...errorMap,
          _: 'Erro ao tentar carregar os agendamentos!'
        });
        toast.error(message);
      }
      setLoading(false);
    } catch (error) {
      setLoading(false);
      toast.error('Erro ao tentar carregar os agendamentos!');
    }
  };

  const handleSubmit = async () => {
    setOpenSchedule(false);
    setLoading(true);
    try {
      if (scheduleDate && patientId) {
        await api.postAppointment({
          patientId,
          date: String(scheduleDate)
        });
        setLoading(false);
        getAppointments();
        toast.success('Agendado com sucesso!');
      }
    } catch (error) {
      setLoading(false);

      toast.error('Erro ao tentar salvar agendamento!');
    }
  };

  const handleOpenSchedule = () => setOpenSchedule(!openSchedule);
  const handleOpenHelp = () => setOpenHelp(!openHelp);

  useEffect(() => {
    getAppointments();
  }, []);

  return (
    <>
      <Modal
        title="Agendar uma consulta"
        cancel="cancelar"
        approve="agendar"
        open={openSchedule}
        onCancel={handleOpenSchedule}
        onClose={handleOpenSchedule}
        onApprove={handleSubmit}
      >
        <S.ScheduleWrapper>
          <S.SelectWrapper>
            <Typography align="center">selecione um paciente</Typography>
            <DropDown
              label="Selecione um paciente"
              fullWidth
              effect="effect"
              options={patients}
              onOption={(e) => setPatientId(e as number)}
            />
          </S.SelectWrapper>
          <S.CalendarWrapper>
            <Typography>selecione uma data</Typography>
            <Calendar initialDay={scheduleDate} onDay={setScheduleDate} />
          </S.CalendarWrapper>
        </S.ScheduleWrapper>
      </Modal>
      <Modal open={openHelp} title="Ajuda" onClose={handleOpenHelp}>
        <S.HelpWrapper>
          <Typography variant="subtitle" align="start">
            Entre em contato em algum de nossos canais
          </Typography>
          <S.ContactsWrapper>
            <Contact title="Telefone" value="(32) 9 1234-5678" />
            <Contact title="Whatsapp" value="(32) 9 1234-5678" />
            <Contact title="E-mail" value="middle.earth@mail.com" />
          </S.ContactsWrapper>
        </S.HelpWrapper>
      </Modal>
      <S.Header>
        <Typography variant="heading-2">Consultas</Typography>
      </S.Header>
      <S.Main>
        <S.LoadingWrapper>
          <Loading loading={loading} size={50} />
        </S.LoadingWrapper>
        {!appointments && !loading && (
          <S.NotFoundWrapper>
            <AppointmentsNotFound />
          </S.NotFoundWrapper>
        )}
        {!!appointments && !loading && (
          <S.AppointmentsWrapper>
            <AppointmentsList appointments={appointments} />
          </S.AppointmentsWrapper>
        )}
      </S.Main>
      <S.Footer>
        <Button border onClick={handleOpenHelp}>
          Ajuda
        </Button>
        <Button disabled={patients.length === 0} onClick={handleOpenSchedule}>
          Agendar consulta
        </Button>
      </S.Footer>
    </>
  );
};

type ContactProps = {
  title: string;
  value: string;
};

const Contact = ({ title, value }: ContactProps) => (
  <S.ContactsBox>
    <Typography component="span" variant="caption" color="blue-light">
      {title}
    </Typography>
    <Typography variant="body">{value}</Typography>
  </S.ContactsBox>
);

export default Home;
