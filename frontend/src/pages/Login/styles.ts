import styled, { css } from 'styled-components';
import media from 'styled-media-query';

export const Wrapper = styled.div`
  ${media.greaterThan('large')`
    display: flex;
    margin: 6rem auto 0;
    padding: 0 14.1rem;
    gap: 32.7rem;
    width: 100%;
    max-width: 136.6rem;
  `}
`;

export const BannerWrapper = styled.div`
  display: none;
  flex-direction: column;
  gap: 6.7rem;
  > svg {
    width: 337.52px;
    height: 265.46px;
  }

  ${media.greaterThan('large')`
    display: flex;
  `}
`;

export const FormWrapper = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;
    position: relative;
    gap: ${theme.spacings.xxhuge};
    padding: 3.5rem ${theme.spacings.medium} 0;

    ${media.greaterThan('large')`
      width: 100%;
      max-width: 23.5rem;
      padding: 0;
      justify-content: center;
      > *:first-child {
        display: none;
      }
    `}
  `}
`;

export const LoginForm = styled.form`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;
    gap: ${theme.spacings.xhuge};
  `}
`;

export const InputsWrapper = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;
    gap: ${theme.spacings.xxlarge};
  `}
`;
