import React, { FormEvent, useState } from 'react';
import { toast } from 'react-toastify';

import Typography from 'components/Typography';
import TextField from 'components/TextField';
import Button from 'components/Button';
import Icon from 'components/Icon';
import Loading from 'components/Loading';

import useForm from 'hooks/useForm';
import api from 'services/api';
import { login } from 'utils/security';
import { findOrDefault } from 'utils/findOrDefault';
import { errorMap } from 'utils/errorMap';

import * as S from './styles';

const Login = () => {
  const [loading, setLoading] = useState(false);
  const email = useForm('email');
  const password = useForm('other');

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    try {
      if (email.validate() && password.validate()) {
        setLoading(true);

        const { name, token, message } = await api.login({
          email: email.value,
          password: password.value
        });

        if (token && name && !message) {
          login({ name, token });
        } else {
          const find = findOrDefault(message, {
            ...errorMap,
            _: 'Erro ao tentar fazer login!'
          });
          toast.error(find);
        }
        setLoading(false);
      }
    } catch (err) {
      setLoading(false);
      toast.error('Erro ao tentar fazer login!');
    }
  };
  return (
    <S.Wrapper>
      <S.BannerWrapper>
        <Typography variant="heading-1">Faça Login</Typography>
        <Icon icon="login" />
      </S.BannerWrapper>
      <S.FormWrapper>
        <Typography variant="heading-1">Faça Login</Typography>
        <S.LoginForm onSubmit={handleSubmit}>
          <S.InputsWrapper>
            <TextField
              label="E-mail"
              placeholder="Digite seu e-mail"
              {...email}
            />
            <TextField
              label={<PasswordLabel />}
              placeholder="Digite sua senha"
              type="password"
              {...password}
            />
          </S.InputsWrapper>
          <Button fullWidth type="submit" disabled={loading}>
            <Loading loading={loading} size={18} fallBack="Entrar" />
          </Button>
        </S.LoginForm>
      </S.FormWrapper>
    </S.Wrapper>
  );
};

const PasswordLabel = () => (
  <>
    Senha <Icon icon="help" color="gray-medium" size={14} />
  </>
);

export default Login;
